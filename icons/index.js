import * as React from 'react';
import { createIconSetFromIcoMoon } from '@expo/vector-icons';
import icoMoonConfig from '../assets/fonts/icomoon/selection.json';
const expoAssetId = require("../assets/fonts/icomoon/fonts/icomoon.ttf");
export default createIconSetFromIcoMoon(icoMoonConfig, 'icomoon', false);

