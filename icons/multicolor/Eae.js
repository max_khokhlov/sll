import React from 'react';
import Svg, { Path } from 'react-native-svg';
/* SVGR has dropped some elements not supported by react-native-svg: style */

const Eae = props => (
  <Svg x={0} y={0} viewBox='0 0 22 14' xmlSpace='preserve' {...props}>
    <Path
      fill={!!props.color_1 ? props.color_1 : '#492F8D'}
      d='M10.9 11.1c-2.2 0-4-1.8-4-4s1.8-4 4-4 4 1.8 4 4-1.8 4-4 4zm0-6.7c-1.5 0-2.7 1.2-2.7 2.7 0 1.5 1.2 2.7 2.7 2.7s2.7-1.2 2.7-2.7c0-1.5-1.2-2.7-2.7-2.7z'
    />
    <Path
      fill={!!props.color_1 ? props.color_1 : '#492F8D'}
      d='M10.9 14.1C6.8 14.1 3 11.7.1 7.5l-.3-.4.3-.4.6-.9 1.1.8c-.1.2-.2.3-.3.5 2.6 3.6 5.9 5.7 9.4 5.7s6.8-2 9.4-5.7c-.1-.1-.2-.3-.3-.4l1-.9.6.9.3.4-.3.4c-2.8 4.2-6.6 6.6-10.7 6.6z'
    />
    <Path
      d='M1.8 6.6l-1-.8C3.6 2.1 7.2 0 10.9 0S18.3 2 21 5.8l-1 .8c-2.5-3.4-5.7-5.2-9.1-5.2S4.4 3.3 1.8 6.6z'
      fill={!!props.color_2 ? props.color_2 : '#ed3f96'}
    />
  </Svg>
);

export default Eae;
