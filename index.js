/*Modules*/
import CreateInvoke from './modules/invoke';
import BarcodeModule from './modules/barcode/barcode.module';
import PermisionsModule from './modules/permissions/permissions.module';

/*Contants*/
import color from './constants/Color';

/*Styles*/
import styles from './styles';

/*Components*/
import { YellowButton, GreyButton, GroupButtons } from './components/buttons';
import { ActivityIndicatorComponent } from './components/activity_indicator';
import { ClickboardText } from './components/clickboard_text';
import {
  Accordion,
  AccordionTitle,
  AccordionContent,
} from './components/accordion';
import { Image } from './components/image/Image';

/*Component Form*/
import {
  InputText,
  InputNumber,
  InputImage,
  Select,
  TreeSelectModal,
  Counter,
} from './components/form';

/*Icons*/
import IcomoonIcons from './icons';
import { Eae } from './icons/multicolor';

module.exports = {
  get BarcodeScanner() {
    return BarcodeModule;
  },
  get Permissions() {
    return PermisionsModule;
  },
  get YellowButton() {
    return YellowButton;
  },
  get GreyButton() {
    return GreyButton;
  },
  get GroupButtons() {
    return GroupButtons;
  },
  get ActivityIndicator() {
    return ActivityIndicatorComponent;
  },
  get CreateInvoke() {
    return CreateInvoke;
  },
  get ClipboardText() {
    return ClickboardText;
  },
  get Image() {
    return Image;
  },
  get Eae() {
    return Eae;
  },
  get Color() {
    return color;
  },
  get SvzStyles() {
    return styles;
  },
  get Accordion() {
    return Accordion;
  },
  get AccordionTitle() {
    return AccordionTitle;
  },
  get AccordionContent() {
    return AccordionContent;
  },
  get IcomoonIcons() {
    return IcomoonIcons;
  },
  get InputText() {
    return InputText;
  },
  get InputNumber() {
    return InputNumber;
  },
  get InputImage() {
    return InputImage;
  },
  get Select() {
    return Select;
  },
  get TreeSelectModal() {
    return TreeSelectModal;
  },
  get Counter() {
    return Counter;
  },
};
