import React from 'react';
import color from '../../constants/Color';
import styles from '../../styles';
import IcomoonComponent from '../../icons';

import { Alert, Text, TouchableOpacity, Clipboard } from 'react-native';

export default class ClickboardText extends React.Component {
  constructor(props) {
    super(props);
  }

  _onPressButton = () => {
    Alert.alert(
      !!this.props.title_modal ? this.props.title_modal : 'Cкопировать текст',
      null,
      [
        {
          text: 'Отмена',
          style: 'cancel',
        },
        {
          text: 'Скопировать',
          onPress: () => Clipboard.setString(this.props.children),
        },
      ],
      { cancelable: false },
    );
  };

  render() {
    let style = !!this.props.style ? this.props.style : {};

    return (
      <TouchableOpacity
        style={{ ...style, flex: 1, flexDirection: 'row' }}
        onPress={this._onPressButton}
      >
        <Text style={styles.text}>{this.props.children}</Text>
        <IcomoonComponent
          name='copy'
          style={{ color: color.$grey, marginStart: 5, marginTop: 3 }}
        />
      </TouchableOpacity>
    );
  }
}
