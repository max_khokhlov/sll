import { Camera } from 'expo-camera';
import React, { Component, PropTypes } from 'react';
import * as Permissions from 'expo-permissions';
import { FontAwesome } from '@expo/vector-icons';
import {
  Alert,
  Dimensions,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import Constants from 'expo-constants';

const type = Camera.Constants.Type.back;
const { width, height } = Dimensions.get('window');

export default class CameraGrid extends React.Component {
  componentDidMount() {
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      // const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
      if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(
          Permissions.CAMERA_ROLL,
        );
        if (newPermission.status !== 'granted') {
          Alert.alert(
            'Ошибка!',
            'Для использования камеры, нужно разрешить приложению ее использовать!',
            [
              {
                text: 'Закрыть',
                style: 'cancel',
              },
            ],
          );
        }
      }
      const permissionCamera = await Permissions.getAsync(Permissions.CAMERA);
      if (permissionCamera.status !== 'granted') {
        const newPermissionCamera = await Permissions.askAsync(
          Permissions.CAMERA,
        );
        if (newPermissionCamera.status !== 'granted') {
          Alert.alert(
            'Ошибка!',
            'Для использования камеры, нужно разрешить приложению ее использовать!',
            [
              {
                text: 'Закрыть',
                style: 'cancel',
              },
            ],
          );
        }
      }
    }
  };

  takePicture = async () => {
    if (this.camera) {
      let photo = await this.camera.takePictureAsync({
        base64: true,
        quality: 0.8,
      });
      return photo;
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={{
            flex: 1,
            position: 'absolute',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: width,
            height: height - 170,
            zIndex: 3,
          }}
        >
          <Image
            source={require('../../assets/images/grid-png-43584.png')}
            fadeDuration={0}
            resizeMode={'stretch'}
            style={{ width: width }}
          />
        </View>

        <Camera
          style={styles.preview}
          ref={ref => {
            this.camera = ref;
          }}
          type={type}
        >
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              zIndex: 4,
              backgroundColor: 'black',
            }}
          >
            <TouchableOpacity
              onPress={this.takePicture.bind(this)}
              style={{ paddingVertical: 15 }}
            >
              <FontAwesome name='camera' size={38} color={'white'} />
            </TouchableOpacity>
          </View>
        </Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'transparent',
  },
  preview: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    zIndex: 2,
  },
  capture: {
    //backgroundColor: '#fff',
    //borderRadius: 5,
    padding: 15,
    //paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});
