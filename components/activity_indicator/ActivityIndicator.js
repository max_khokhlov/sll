import React from 'react';
import color from '../../constants/Color';
import styles from '../../styles';

import { ActivityIndicator, View, Text } from 'react-native';

export default class extends React.Component {
  render() {
    const { style = {} } = this.props;

    return (
      <View style={{ ...styles.activity_block, ...style }}>
        <ActivityIndicator
          size={!!this.props.size ? this.props.size : 'large'}
          color={!!this.props.color ? this.props.color : color.$pink}
        />
        {!!this.props.text ? (
          <Text style={{ marginTop: 15, ...styles.text }}>
            {this.props.text}...
          </Text>
        ) : null}
      </View>
    );
  }
}
