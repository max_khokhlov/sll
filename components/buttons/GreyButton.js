import React from 'react';
import color from '../../constants/Color';
import styles from '../../styles';
import { Dimensions, TouchableOpacity, View, Text } from 'react-native';
const width = Dimensions.get('window').width;

export default class GreyButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      width: width / 2,
    };
  }

  render() {
    return (
      <TouchableOpacity
        underlayColor='white'
        onPress={() => {
          this.props.onPress();
        }}
      >
        <View
          style={
            this.props.style
              ? this.props.style
              : {
                  ...styles.grey_button,
                  width: !!this.props.width
                    ? this.props.width
                    : this.state.width,
                }
          }
        >
          <Text
            style={
              this.props.style_text
                ? this.props.style_text
                : { ...styles.text_btn, color: color.$purple }
            }
          >
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
