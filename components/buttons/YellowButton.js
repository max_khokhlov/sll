import React, { PropTypes } from 'react';
import { Text, View, Dimensions, TouchableOpacity } from 'react-native';

import styles from '../../styles';
const width = Dimensions.get('window').width;

export default class YellowBtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      width: width / 1.5,
    };
  }

  render() {
    return (
      <TouchableOpacity
        style={{ flexDirection: 'row', justifyContent: 'center' }}
        underlayColor='white'
        onPress={() => {
          this.props.onPress();
        }}
      >
        <View
          style={
            this.props.style
              ? this.props.style
              : {
                  ...styles.btn_yellow,
                  width: !!this.props.width
                    ? this.props.width
                    : this.state.width,
                }
          }
        >
          <Text
            style={
              this.props.style_text ? this.props.style_text : styles.text_btn
            }
          >
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
