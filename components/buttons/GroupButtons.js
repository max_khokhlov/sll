import React from 'react';
import color from '../../constants/Color';
import styles from '../../styles';
import { Dimensions, Platform } from 'react-native';

const widthAll = Dimensions.get('window').width; //full width

import { TouchableOpacity, View, Text, ScrollView } from 'react-native';

export default class GroupButtons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      buttons: this.props.buttons,
      refElem: null,
    };
  }

  getButton = (style_btn, data, value, index) => {
    let refButton = null;

    if (data.value === value) {
      return (
        <TouchableOpacity key={index} underlayColor='white'>
          <View
            style={{
              ...style_btn,
              backgroundColor: color.$yellow,
              color: color.$black,
            }}
          >
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'SvyaznoySans-Medium',
                color: color.$black,
                fontSize: 14,
              }}
            >
              {data.label}
            </Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <TouchableOpacity
          key={index}
          underlayColor='white'
          ref={ref => (refButton = ref)}
          onPress={() => this.pressToButton(index, refButton, data.value)}
        >
          <View style={style_btn}>
            <Text
              style={{
                textAlign: 'center',
                fontFamily: 'SvyaznoySans-Medium',
                color: color.$grey,
                fontSize: 14,
              }}
            >
              {data.label}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
  };

  pressToButton = (index, refButton, value) => {
    if (Platform.OS === 'ios') {
      refButton.measure((fx, fy, width, height, px, py) => {
        scrollX = fx - ((widthAll - width) / 2).toFixed(0) - 10;
        // TODO
        this.state.refElem.scrollTo &&
          this.state.refElem.scrollTo({
            x: scrollX > 0 ? scrollX : 0,
            y: 0,
            animated: true,
          });
      });
    }

    this.props.onPress(value);
  };

  render() {
    if (!!this.props.buttons === false) return null;

    if (!!this.props.style) {
      return (
        <ScrollView
          style={{
            backgroundColor: color.$white,
            height: 60,
            paddingVertical: 10,
          }}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          ref={ref => (this.state.refElem = ref)}
        >
          {this.state.buttons.map((item, index) => {
            return this.getButton(
              this.props.style,
              item,
              this.props.value,
              index,
            );
          })}
        </ScrollView>
      );
    }

    return (
      <View style={styles.group_block} ref={ref => (this.state.refElem = ref)}>
        {this.state.buttons.map((item, index) => {
          if (index === 0) {
            return this.getButton(
              styles.first_btn,
              item,
              this.props.value,
              index,
            );
          } else if (index === this.state.buttons.length - 1) {
            return this.getButton(
              styles.last_btn,
              item,
              this.props.value,
              index,
            );
          } else {
            return this.getButton(
              styles.other_btn,
              item,
              this.props.value,
              index,
            );
          }
        })}
      </View>
    );
  }
}
