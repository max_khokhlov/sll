import React, { Component } from 'react';
import { View, TextInput, TouchableOpacity } from 'react-native';
import styles from '../../styles';
import color from '../../constants/Color';
import IcomoonIcons from '../../icons';

class Counter extends Component {
  state = {
    value: 1,
  };

  pressToButton = async val => {
    let { value } = this.state;
    let { max } = this.props;

    if (value === 1 && val === -1) {
      return;
    }

    if (value === parseInt(max)) return;

    await this.setState({
      value: value + val,
    });
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevState.value !== this.state.value) {
      this.props.onChange ? this.props.onChange(this.state.value) : {};
    }
  }

  getButton = (style_btn, title, value) => {
    let refButton = null;

    return (
      <TouchableOpacity
        key={value}
        underlayColor='white'
        ref={ref => (refButton = ref)}
        onPress={() => this.pressToButton(value)}
      >
        <View style={style_btn}>{title}</View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        {this.getButton(
          styles.counter.left_button,
          <IcomoonIcons name={'minus'} size={5} color={color.$black} />,
          -1,
        )}
        <TextInput
          style={styles.counter.input}
          keyboardType={'numeric'}
          caretHidden={true}
          editable={false}
          onChange={value =>
            this.setState(state => ({
              value: state.value + value,
            }))
          }
          value={this.state.value.toString()}
        />
        {this.getButton(
          styles.counter.right_button,
          <IcomoonIcons name={'plus'} size={15} color={color.$black} />,
          1,
        )}
      </View>
    );
  }
}

export default Counter;
