import { PureComponent } from 'react';
import IcomoonComponent from '../../icons';
import color from '../../constants/Color';
import { Button, Modal, ScrollView, Text, View } from 'react-native';
import React from 'react';

class TreeSelectModal extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      visibleModal: false,
      values: {
        name: null,
        id: null,
      },
    };
  }

  _onClick = ({ item, routes }) => {};

  _onClickLeaf = ({ item, routes }) => {
    let { values } = this.state;

    values = {
      name: item.name,
      id: item.id,
    };

    this.setState({ values });

    this.props.change(values);

    this.setState({ visibleModal: false });
  };

  visibleModal = () => {
    let disabled = !!this.props.disabled;

    if (!disabled) {
      this.setState({ visibleModal: true });
    }
  };

  icon = open => {
    if (open) {
      return (
        <IcomoonComponent
          name='arrow'
          size={10}
          color={color.$black}
          style={{ left: -12, transform: [{ rotate: '90deg' }] }}
        />
      );
    }

    return (
      <IcomoonComponent
        name='arrow'
        size={10}
        color={color.$black}
        style={{ left: -12 }}
      />
    );
  };

  render() {
    let treeselectData = !!this.props.data ? this.props.data : [];

    let disabled = !!this.props.disabled;

    return (
      <View>
        <Button
          title={
            !!this.props.button_title ? this.props.button_title : 'Выбрать'
          }
          color={disabled ? color.$grey_w : color.purple}
          onPress={this.visibleModal}
        />

        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.visibleModal}
        >
          <View style={styles.modal}>
            <View
              style={{ width: width, height: 30, flex: 1, marginBottom: 20 }}
            >
              <View
                style={{
                  width: 100,
                  position: 'absolute',
                  right: 10,
                  top: -20,
                }}
              >
                <Button
                  title='Закрыть'
                  onPress={() => {
                    this.setState({ visibleModal: false });
                  }}
                />
              </View>
            </View>
            <Text style={styles.modal_title}>
              {!!this.props.button_title ? this.props.button_title : 'Выбрать'}
            </Text>
            <ScrollView>
              <View style={{ width: width, paddingBottom: 80 }}>
                <TreeSelect
                  data={treeselectData}
                  isShowTreeId={false}
                  itemStyle={{
                    flex: 1,
                    fontSize: 16,
                    paddingLeft: 10,
                    paddingRight: 10,
                    color: color.$purple,
                  }}
                  selectedItemStyle={{
                    backgroudColor: color.$grey_header,
                    paddingLeft: 10,
                    paddingRight: 10,
                    fontSize: 16,
                    color: color.$pink,
                  }}
                  onClickLeaf={
                    !!this.props.onClickLeaf
                      ? this.props.onClickLeaf
                      : this._onClickLeaf
                  }
                  onClick={
                    !!this.props.onClickItem
                      ? this.props.onClickItem
                      : this._onClick
                  }
                  treeNodeStyle={{
                    openIcon: this.icon(true),
                    closeIcon: this.icon(),
                  }}
                />
              </View>
            </ScrollView>
          </View>
        </Modal>

        {!!this.state.values.name && !disabled ? (
          <Text style={styles.selected_text}>
            Выбрано: {this.state.values.name}
          </Text>
        ) : null}
      </View>
    );
  }
}

export default TreeSelectModal;
