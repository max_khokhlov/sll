import React from 'react';
import { Alert, Button, Image, Text, View } from 'react-native';
import { GreyButton } from '../buttons';
import IcomoonComponent from '../../icons';
import color from '../../constants/Color';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

class InputImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
  }

  render() {
    let { image } = this.state;

    return (
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={styles.block_btn}>
          {!!this.props.style ? (
            <Button
              style={this.props.style}
              title={
                !!this.props.title ? this.props.title : 'Загрузить изображение'
              }
              onPress={this._getAlert}
            />
          ) : (
            <GreyButton
              title={
                !!this.props.title ? this.props.title : 'Загрузить изображение'
              }
              onPress={this._getAlert}
              width={width - 40}
            />
          )}
        </View>

        {!!this.props.image_block && image ? (
          <View style={{ flex: 1 }}>
            <Text
              style={{
                flex: 1,
                position: 'absolute',
                right: 10,
                top: 10,
                zIndex: 9,
              }}
              onPress={this._deleteImage}
            >
              <IcomoonComponent name='delete' size={35} color={color.$red} />
            </Text>
            <Image
              source={{ uri: image }}
              style={
                !!this.props.image_style
                  ? this.props.image_style
                  : { width: 200, height: 200 }
              }
            />
          </View>
        ) : null}
      </View>
    );
  }

  _deleteImage = () => {
    this.setState({
      image: null,
    });

    this.props.change(null);
  };

  _getAlert = () => {
    return Alert.alert(
      'Загрузить изображение',
      'Выберите источник изображения',
      [
        { text: 'Камера', onPress: () => this._pickImageFromCamera() },
        {
          text: 'Закрыть',
          style: 'cancel',
        },
        { text: 'Хранилище', onPress: () => this._pickImageFromLibrary() },
      ],
      { cancelable: false },
    );
  };

  componentDidMount() {
    this.getPermissionAsync();
  }

  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      // const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL);
      if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(
          Permissions.CAMERA_ROLL,
        );
        if (newPermission.status !== 'granted') {
          Alert.alert(
            'Ошибка!',
            'Для использования хранилища, нужно разрешить приложению его использовать!',
            [
              {
                text: 'Закрыть',
                style: 'cancel',
              },
            ],
          );
        }
      }
      const permissionCamera = await Permissions.getAsync(Permissions.CAMERA);
      if (permissionCamera.status !== 'granted') {
        const newPermissionCamera = await Permissions.askAsync(
          Permissions.CAMERA,
        );
        if (newPermissionCamera.status !== 'granted') {
          Alert.alert(
            'Ошибка!',
            'Для использования камеры, нужно разрешить приложению ее использовать!',
            [
              {
                text: 'Закрыть',
                style: 'cancel',
              },
            ],
          );
        }
      }
    }
  };

  _pickImageFromLibrary = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      aspect: [1, 1],
    });

    if (!result.cancelled) {
      this.props.change(result.uri);
      this.setState({ image: result.uri });
    }
  };

  _pickImageFromCamera = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      aspect: [1, 1],
    });

    if (!result.cancelled) {
      this.props.change(result.uri);
      this.setState({ image: result.uri });
    }
  };
}

export default InputImage;
