import React from 'react';
import { TextInput, View } from 'react-native';
import styles from '../../styles';

class InputText extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
    };
  }

  render() {
    let { value } = this.state;

    return (
      <View style={{ flexDirection: 'row' }}>
        <TextInput
          value={value}
          style={styles.input}
          onChangeText={value => {
            this.setState({
              value: value,
            });
            this.props.change(value);
          }}
          placeholder={this.props.placeholder}
        ></TextInput>
        {!!this.props.icon ? this.props.icon : null}
      </View>
    );
  }
}

export default InputText;
