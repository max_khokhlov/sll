import { PureComponent } from 'react';
import color from '../../constants/Color';
import { View } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import React from 'react';

class Select extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      values: null,
    };
  }

  setValueNull() {
    let { values } = this.state;

    if (values !== null) {
      values = null;
      this.setState(values);
    }
  }

  render() {
    // if (this.props.placeholder === 'Выбрать город') console.log('select_city', this.props)

    let style = selectStyles;
    let disabled = !!this.props.disabled ? this.props.disabled : false;

    if (typeof this.props.change_value_on_disable !== 'undefined') {
      disabled = !!this.props.change_value_on_disable;
    }

    if (disabled) {
      this.setValueNull();
      style = selectDisabledStyles;
    }

    const placeholder = {
      label: !!this.props.placeholder ? this.props.placeholder : 'Выбрать',
      value: null,
      color: disabled ? color.$grey_header : color.$black,
    };

    return (
      <View style={{ flexDirection: 'row', width: '100%' }}>
        <RNPickerSelect
          value={this.state.values}
          placeholder={placeholder}
          style={!!this.props.style ? this.props.style : style}
          disabled={disabled}
          useNativeAndroidPickerStyle={false}
          doneText={!!this.props.done ? this.props.done : 'Выбрать'}
          onValueChange={value => {
            this.setState({
              values: value,
            });
            this.props.change(value);
          }}
          items={this.props.items}
        />
        {!!this.props.icon ? this.props.icon : null}
      </View>
    );
  }
}

export default Select;
