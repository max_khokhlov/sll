import React, { Component } from 'react';
import { Text, View } from 'react-native';

class AccordionContent extends Component {
  render() {
    return (
      <View>
        {this.props.children({
          content: this.props.content,
          active: this.props.active,
        })}
      </View>
    );
  }
}

export default AccordionContent;
