import React, { Component } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { ActivityIndicatorComponent as ActivityIndicator } from '../../components/activity_indicator';
import IcomoonIcons from '../../icons';
import Color from '../../constants/Color';
import SvzStyles from '../../styles';

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      loading: false,
    };
  }

  getAccordionContent = () => {
    const element = React.cloneElement(this.props.content, {
      content: this.props.item.content,
      active: this.state.active,
    });

    return <View>{element}</View>;
  };

  getAccordionTitle = () => {
    const element = React.cloneElement(this.props.title, {
      title: this.props.item.title,
      active: this.state.visible,
    });

    return <View>{element}</View>;
  };

  handleVisible = () => {
    this.setState({ visible: !this.state.visible });
  };

  render() {
    const { visible, loading } = this.state;

    return (
      <View>
        <TouchableOpacity style={styles.item} onPress={this.handleVisible}>
          <View>{this.getAccordionTitle()}</View>
          {loading ? (
            <ActivityIndicator
              style={styles.childrenIndicator}
              size='small'
              color={color.$grey}
            />
          ) : (
            <IcomoonIcons
              name='arrow'
              style={visible ? styles.icon_active : styles.icon}
              size={10}
            />
          )}
          <View style={styles.separator} />
        </TouchableOpacity>
        <View>{visible && this.getAccordionContent()}</View>
      </View>
    );
  }
}

export const styles = {
  item: {
    paddingVertical: 15,
    justifyContent: 'center',
  },
  itemChildPeoples: {
    paddingLeft: 15,
    paddingRight: 10,
    backgroundColor: '#fbfbfc',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemChildPeopleInfo: {
    width: '80%',
    paddingVertical: 15,
    marginLeft: 15,
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: Color.$grey_w,
  },
  itemChildDepts: {
    paddingHorizontal: 10,
  },
  title: {
    paddingHorizontal: 15,
    ...SvzStyles.text,
    fontSize: 20,
    color: Color.$grey_text,
  },
  title_active: {
    paddingHorizontal: 15,
    ...SvzStyles.text,
    fontSize: 20,
    color: Color.$pink,
  },
  titleChild: {
    ...SvzStyles.text,
    fontSize: 16,
    color: Color.$black,
    paddingRight: 15,
  },
  titleChildActive: {
    ...SvzStyles.text,
    fontSize: 16,
    color: Color.$pink,
    paddingRight: 15,
  },
  titlePeopleChild: {
    ...SvzStyles.text,
    fontSize: 18,
    color: Color.$black,
  },
  jobChild: {
    ...SvzStyles.text,
    fontSize: 14,
    color: Color.$grey_text,
  },
  separator: {
    bottom: -15,
    borderBottomWidth: 1,
    borderBottomColor: Color.$grey_w,
  },
  separator_peoples: {
    bottom: -15,
    borderBottomWidth: 1,
    borderBottomColor: Color.$grey,
  },
  icon: {
    position: 'absolute',
    right: 5,
    color: Color.$grey_text,
    transform: [{ rotate: '90deg' }],
  },
  icon_active: {
    position: 'absolute',
    right: 5,
    color: Color.$pink,
    transform: [{ rotate: '-90deg' }],
  },
  childrenIndicator: {
    position: 'absolute',
    right: 8,
  },
};

export default Item;
