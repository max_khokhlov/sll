import React, { Component } from 'react';
import {
  FlatList,
  SafeAreaView,
  View,
  Text,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import { ActivityIndicatorComponent } from './../activity_indicator';
import Item from './Item';

class Accordion extends Component {
  constructor(props) {
    super(props);

    this.viewabilityConfig = {
      minimumViewTime: 100,
      waitForInteraction: false,
      viewAreaCoveragePercentThreshold: 95,
    };

    this.state = {
      content: null,
      title: null,
    };
  }

  uuid = () => {
    return `f${(~~(Math.random() * 1e8)).toString(16)}`;
  };

  componentDidMount() {
    const children = this.props.children;

    if (children.length < 2)
      return Error(
        "Empty children. Add 'AccordionTitle' and 'AccordionContent'",
      );

    this.setState({ title: children[0] });
    this.setState({ content: children[1] });
  }

  render() {
    if (this.props.loading) {
      return <ActivityIndicatorComponent text={'Загружаем данные'} />;
    }

    if (!this.state.title || !this.state.content) return null;

    return (
      <ScrollView>
        <SafeAreaView>
          <FlatList
            viewabilityConfig={this.viewabilityConfig}
            data={this.props.data || []}
            listKey={this.uuid()}
            renderItem={({ item }) => (
              <Item
                item={item}
                title={this.state.title}
                content={this.state.content}
              />
            )}
            keyExtractor={item => {
              return item[this.props.keyName] || item.id;
            }}
          />
        </SafeAreaView>
      </ScrollView>
    );
  }
}

export default Accordion;
