import React, { Component } from 'react';
import { View, Text } from 'react-native';

class AccordionTitle extends Component {
  render() {
    return (
      <View>
        {this.props.children({
          title: this.props.title,
          active: this.props.active,
        })}
      </View>
    );
  }
}

export default AccordionTitle;
