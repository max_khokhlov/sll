import { Image as ImageRN } from 'react-native';
import React from 'react';

export const Image = ({ src, width, height, style }) => {
  const style_props = !!style ? style : {};
  if (!!src) {
    let src_image = './../../assets/images/' + 'bg_qrcode.png';
    return (
      <ImageRN
        source={require(src_image)}
        style={{
          width: !!width ? width : 100,
          height: !!height ? height : 100,
          ...style_props,
        }}
      ></ImageRN>
    );
  }
};
