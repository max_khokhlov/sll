import * as Permissions from 'expo-permissions';

export default class PermissionsModule{

    static getPermissionsAsync = async (permissions_module) => {

        let module = false;

        switch (permissions_module){
            case 'CAMERA':
                module = Permissions.CAMERA;
                break;
            case 'AUDIO_RECORDING':
                module = Permissions.AUDIO_RECORDING;
                break;
            case 'CONTACTS':
                module = Permissions.CONTACTS;
                break;
            case 'CAMERA_ROLL':
                module = Permissions.CAMERA_ROLL;
                break;
            case 'CALENDAR':
                module = Permissions.CALENDAR;
                break;
            case 'REMINDERS':
                module = Permissions.REMINDERS;
                break;
            case 'SYSTEM_BRIGHTNESS':
                module = Permissions.SYSTEM_BRIGHTNESS;
                break;
            default:
                return false;
        }

        return await Permissions.askAsync(module);
    };

}