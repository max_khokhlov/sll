import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Modal,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import PermissionsModule from '../../modules/permissions/permissions.module';
import { YellowButton } from '../../components/buttons';
import { ActivityIndicatorComponent as ActivityIndicator } from '../../components/activity_indicator';
import color from '../../constants/Color';
import IcomoonComponent from '../../icons';
import styles from '../../styles';
import { Image } from '../../components/image/Image';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

let styles_local = {
  block_btn: {
    position: 'absolute',
    top: 25,
    width: '100%',
    height: 60,
    paddingTop: 10,
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    zIndex: 20,
    paddingBottom: 10,
  },
};

export default class BarcodeModule extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
    modalVisible: false,
  };

  constructor(props) {
    super(props);
  }

  async componentDidMount() {
    this.getPermissionsAsync();
  }

  getPermissionsAsync = async () => {
    const { status } = await PermissionsModule.getPermissionsAsync('CAMERA');
    this.setState({
      hasCameraPermission: status === 'granted',
      modalVisible: status === 'granted',
    });
  };

  _closeModal = () => {
    this.setState({ modalVisible: false });
    if (!!this.props.onClose) {
      this.props.onClose();
    }
  };

  render() {
    const { hasCameraPermission, scanned } = this.state;
    const { text, image, closable, loading } = this.props;

    if (hasCameraPermission === null) {
      return <View></View>;
    }

    if (hasCameraPermission === false) {
      return <Text>Нет доступа к камере</Text>;
    }
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'flex-end',
        }}
      >
        <Modal
          animationType='slide'
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View
            style={{
              flex: 1,
              position: 'absolute',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              width: '100%',
              height: '100%',
              margin: 0,
              zIndex: 8,
            }}
          >
            {loading ? (
              <ActivityIndicator
                style={styles_component.activity_indicator}
                color={'#ffffff'}
              />
            ) : null}
            {image ? (
              image
            ) : (
              <Image
                src={'barcode_frame.png'}
                width={width}
                height={height}
                style={{ opacity: 0.95 }}
              ></Image>
            )}
            {text ? (
              <View
                style={{
                  position: 'absolute',
                  bottom: 150,
                  width: 150,
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexWrap: 'wrap',
                }}
              >
                <Text
                  style={{
                    textAlign: 'center',
                    color: color.$white,
                    marginTop: 8,
                    ...styles.text,
                  }}
                >
                  {text}
                </Text>
              </View>
            ) : null}
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
              bottom: 20,
              width: '100%',
              zIndex: 9,
            }}
          >
            {typeof closable === 'undefined' || closable ? (
              <TouchableOpacity
                style={{
                  padding: 10,
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  textAlign: 'center',
                }}
                onPress={this._closeModal}
              >
                <IcomoonComponent name='close' size={25} color={color.$white} />
                <Text
                  style={{
                    color: color.$white,
                    marginTop: 8,
                    ...styles.text,
                    textAlign: 'center',
                  }}
                >
                  ВЫЙТИ
                </Text>
              </TouchableOpacity>
            ) : null}
          </View>
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={StyleSheet.absoluteFillObject}
          />
          {scanned && (
            <View style={styles_local.block_btn}>
              <YellowButton
                title={'Сканировать еще'}
                onPress={() => this.setState({ scanned: false })}
              />
            </View>
          )}
        </Modal>
      </View>
    );
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({ scanned: true });
    if (!!this.props.onChange) {
      this.props.onChange({ type, data });
    }
    // alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  };
}

const styles_component = {
  activity_indicator: {
    position: 'absolute',
    top: 15,
    width: width,
    height: height,
    justifyContent: 'center',
    alignItems: 'center',
  },
};
